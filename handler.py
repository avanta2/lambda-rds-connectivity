import sys
import logging
import psycopg2
import json
import os

# rds settings
rds_host  = os.environ['DATABASE_ENDPOINT']
rds_username = os.environ['DATABASE_USER']
rds_user_pwd = os.environ['DATABASE_PASSWORD']
rds_db_name = os.environ['DATABASE_NAME']

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn_string = "host=%s user=%s password=%s dbname=%s" % \
                  (rds_host, rds_username, rds_user_pwd, rds_db_name)
    conn = psycopg2.connect(conn_string)
except:
    logger.error("ERROR: Could not connect to Postgres instance.")
    # sys.exit()

logger.info("SUCCESS: Connection to RDS Postgres instance succeeded")

def hello(event, context):

    query = """select first_name, last_name
            from employees
            order by 1"""

    logger.info("connecting to RDS - " + rds_host + 'and database ' + rds_db_name+ 'with creds = ' + rds_username)

    with conn.cursor() as cur:
        rows = []
        cur.execute(query)
        for row in cur:
            rows.append(row)

    strrows = ','.join(str(v) for v in rows)
    logger.info('DB ROWS: %s' % strrows)

    return { 'statusCode': 200, 'body': strrows}
