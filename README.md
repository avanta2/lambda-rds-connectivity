# Lambda-RDS-Connectivity

Python Lambda function deployed using Serverless Framework to connect and lookup data from RDS postgres DB in private subnet.

## Pre-requisites

- [ ] Serverless Framework installed
- [ ] RDS Postgres DB setup in a VPC in private Subnet
- [ ] SSM Parameters set containing details about the Subnet and RDS endpoint, credentials and lambda execution role ARN
- [ ] AWS Credentials (Access Key and Secret Access Key) set locally
- [ ] Security Groups configured both on the Lambda and on the DB

## Deploy
To Deploy the application run
```
sls deploy
```

## Database Schema

The function queries the `employees` table in the `employees` Database

## Application
The serverless framework deploys the lambda function and associates it with the private VPC subnets provided in the SSM parameters.
It also setsup the API Gateway and associates it with the lambda-proxy. On execution the function attempts to make the connection
with the DB and pull the records.

## Test and Deploy

To test, call the HTTPS API Gateway endpoint created by the deployment.
You should see something below as the output.

```
('Yusuf', 'Cochinwala')
```